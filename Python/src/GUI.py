from Tkinter import *
import sqlite3 as lite
from ttk import *
from PIL import Image, ImageTk
import sys

app = Tk()
menubar = Menu(app)
filemenu = Menu(menubar, tearoff = 0)
filemenu.add_command(label="Exit", command = app.quit)
menubar.add_cascade(label="File", menu=filemenu)

app.config(menu=menubar)
Series = ["Select a Manga"]
variable = StringVar(app)
conn = None

try:
	conn= lite.connect('C:\\sqlite\\manga_central.db')
	curs = conn.cursor()
	values = curs.execute('SELECT \"Name\" FROM Series ORDER BY \"Name\"')
	for value in values:
		Series.append(value[0])
except lite.Error, e:
    print "Error %s:" % e.args[0]
    sys.exit(1)
finally:
    if conn:
        conn.close()
        print 'done'

def go(var):
	print 'value is: ', var

variable.set("Select a Manga")
w = OptionMenu(app, variable, *Series, command=go)
#w.command = go()
w.config(width= 40)


#options=OptionMenu(app, variable, tup)
w.pack()

#button = Button(app, text="Go", command=go)
#button.pack()

app.mainloop()