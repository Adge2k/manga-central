from bs4 import BeautifulSoup
import urllib
import sqlite3 as lite

class Scraper(object):
	
	def __init__(self, curs, conn):
		self.logf = open('Scraper.log','w')
		self.curs = curs
		self.conn = conn
		self.url = 'http://www.mangareader.net/alphabetical'
		self.baseUrl = 'http://www.mangareader.net'
		self.getSeries()

	def getGenre(self, soup):
		genre_listing = soup.findAll('span', {'class':'genretags'})
		genre = []
		for genres in genre_listing:
			for type in genres:
				genre.append(type.encode('ascii', 'ignore'))
		del genre_listing
		return genre

	def getSeries(self):
		getChapters = self.getChapters
		htmltext = urllib.urlopen(self.url).read()
		soup = BeautifulSoup(htmltext)
		index = soup.findAll('ul', {'class':"series_alpha"})
		del soup
		del htmltext
		manga_link = []
		manga_name = []
		for section in index:
			for series in section.findAll('a', href=True):
				manga_link.append(series['href'].encode('ascii', 'ignore'))
				manga_name.append(series.contents[0].encode('ascii', 'ignore'))
		del index
		print len(manga_name)
		map(getChapters, manga_link, manga_name)
		

	def getChapters(self, manga_link, manga_name):
		execute = self.curs.execute
		commit = self.conn.commit
		fetchone = self.curs.fetchone
		success = False
		Series = False
		getPages = self.getPages
		while not success:
			try:
				success = True
				htmltext = urllib.urlopen(self.baseUrl+manga_link).read()
				soup = BeautifulSoup(htmltext)
				chapterlist = soup.findAll('div', {"id":"chapterlist"})
				manga_name = manga_name.replace("'", "\'", 20).replace('"', '\"', 20)
				execute('SELECT * FROM Series WHERE Name = \"' + manga_name + '\"' )
				if fetchone():
					if not Series:
						print manga_name, 'series exists'
						Series = True	
				else:
					description = soup.find('div', {'id':'readmangasum'}).find('p')
					if len(description.contents) > 0:
						desc = description.contents[0].encode('ascii', 'ignore').replace("'", "\'", 26).replace('"', '\'', 26)
					else:
						desc = "NULL"
					genre = self.getGenre(soup)
					cover = soup.find('div', {'id':'mangaimg'})
					manga_properties = soup.find('div', {'id':'mangaproperties'})
					genre = ', '.join(genre)
					cover = cover.find('img')['src'].encode('ascii','ignore')
					p = manga_properties.findAll('td', {'class':'propertytitle'})
					for i in p:
						prop = i.contents[0].encode('ascii','ignore')
						print 'also gets here'
						if prop == ('Alternate Name:'):
							print 'check'
							test = i.findNext('td')						
							if len(test.contents) > 0:
								altName = test.contents[0].encode('ascii','ignore').replace('"', '\'', 20).replace("'", '\'', 20).replace(':', ' - ')
							else:
								altName = 'NULL'
							#print altName
						elif prop == ('Year of Release:'):
							test = i.findNext('td')
							if len(test.contents) > 0:
								yRelease =  test.contents[0].encode('ascii','ignore')
							else:
								yRelease = 'NULL'
							#print yRelease
						elif prop == ('Status:'):
							test = i.findNext('td')
							if len(test.contents) > 0:
								status =  test.contents[0].encode('ascii','ignore').replace('"', '\'', 20)
								#print status
							else:
								status = 'NULL'
						elif prop == ('Author:'):
							test = i.findNext('td')
							if len(test.contents) > 0:
								author =  test.contents[0].encode('ascii','ignore').replace('"', '\'', 20)
							else:
								author = 'NULL'
							#print author
						elif prop == ('Artist:'):
							test = i.findNext('td')
							if len(test.contents) > 0:
								artist =  test.contents[0].encode('ascii','ignore').replace('"', '\'', 20)
							else:
								artist = 'NULL'
							#print artist
						elif prop == ('Reading Direction:'):
							test = i.findNext('td')						
							if len(test.contents) > 0:
								direction =  test.contents[0].encode('ascii','ignore').replace('"', '\'', 20)
								#print direction
							else:
								direction = 'NULL'
					try:
						print manga_name
						execute('INSERT INTO Series VALUES( NULL, \"' + manga_name + '\" , \"' + altName + '\", \"' + desc + '\", \"' + genre + '\", \"' + direction + '\", \"' + status + '\", 1 )')
						values = execute('SELECT \"Manga ID\" FROM Series WHERE Name = \"' + manga_name + '\"')
						for value in values:
							ID = value[0]
						execute('INSERT INTO Mangaka VALUES(' + str(ID) + ', \"' + author + '\", \"' + artist + '\" , \"' + str(yRelease) + '\" , \"' + str(manga_link) + '\" , \"' + str(cover) + '\")')
						commit()
					except SyntaxError:
						print manga_name, 'gives syntax error'
				for chapters in chapterlist:
					for chapter in chapters.findAll('a', href=True):
						d = chapter.parent
						d = d.findNext('td')
						content = chapter.contents[0].encode('ascii', 'ignore').replace('"', '\'', 20)
						execute('SELECT * FROM Chapters WHERE \"Chapter Text\" = \"' + content + '\"')
						if not self.curs.fetchone():
							print content
							href = chapter['href'].encode('ascii','ignore')
							if len(d.contents) >0:
								date_added = d.contents[0].encode('ascii','ignore')
							else:
								date_added = "NULL"
							getPages(href, content, date_added, manga_name)
						else:
							print content, 'already exists'
				del chapterlist
			except:
				success = False
				self.logf.write("Network Error accessing " + self.baseUrl+manga_link + "\n")
			

	def getPages(self, link, text, date_added, manga_name):
		commit = self.conn.commit
		execute = self.curs.execute
		count = 0
		success = False
		while not success:
			try:
				success = True
				htmltext = urllib.urlopen(self.baseUrl+link).read()
				soup = BeautifulSoup(htmltext)
				pages = soup.findAll('option')
				p_total = len(pages)
				del htmltext
				del soup
				try:
					values = execute('SELECT \"Manga ID\" FROM Series WHERE Name = \"' + manga_name + '\"')
					for value in values:
						ID = value[0]
					execute('INSERT INTO Chapters VALUES ('+str(ID)+', NULL, \"' + date_added + '\", \"' + text + '\", \"' + link + '\", ' + str(p_total) +')' )
				except SyntaxError:
					self.logf.write("SyntaxError: " + text + "\n")
				for page in pages:
					plink = page['value'].encode('ascii','ignore')
					count+=1
					succeed = False
					while not succeed:
						try:
							succeed = True
							htmltext = urllib.urlopen(self.baseUrl + plink).read()
							soup = BeautifulSoup(htmltext)
							pic = soup.find('div', {'id':'imgholder'})
							del htmltext
							del soup
							values = execute('SELECT \"Chapter ID\" FROM Chapters WHERE \"Chapter Text\" = \"' + text + '\"')
							for value in values:
								ch_ID = value[0]
							u = pic.find('img')['src'].encode('ascii','ignore')
							execute('INSERT INTO Pages VALUES (NULL, '+str(ch_ID)+', ' + str(count) + ', \"' + u + '\")' )
							print count, '/', p_total, 'pages done'
						except:
							succeed = False
							self.logf.write("Network Error accessing " + self.baseUrl+page['value'].encode('ascii','ignore') + "\n")
				commit()
			except:
				success = False
				self.logf.write("Network Error accessing " + self.baseUrl+link + "\n")

	
