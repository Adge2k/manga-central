from Scraperold import *
import sqlite3 as lite
import sys

conn = None

try:
	conn= lite.connect('C:\\sqlite\\test.db')
	curs = conn.cursor()
	scrape = Scraper(curs, conn)
except lite.Error, e:
    print "Error %s:" % e.args[0]
    sys.exit(1)
finally:
    if conn:
        conn.close()
        print 'done'
