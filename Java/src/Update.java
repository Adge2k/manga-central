import java.io.IOException;
import java.sql.Connection;

import javax.swing.JOptionPane;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Update extends Scraper implements Runnable{

	Update(Connection c) {
		super(c);
		// TODO Auto-generated constructor stub
	}
	
	void getNewManga(){
		try{		
			Document doc = Jsoup.connect("http://www.mangareader.net/").timeout(10*1000).get();
			Elements contents = doc.getElementsByClass("chapter");
			for(Element content:contents){
				//content.select("strong").text();
				System.out.println(content.text().replace('"', '\'').trim());
				getChapters(content.attr("href").toString(), content.text().replace('"', '\'').trim());
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("update done ...");
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		try{		
			Document doc = Jsoup.connect("http://www.mangareader.net/").timeout(10*1000).get();
			Elements contents = doc.getElementsByClass("chapter");
			for(Element content:contents){
				//content.select("strong").text();
				System.out.println(content.text().replace('"', '\'').trim());
				getChapters(content.attr("href").toString(), content.text().replace('"', '\'').trim());
			}
			JOptionPane.showMessageDialog(null, "Update Complete...");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("update done ...");
		
	}

}
