import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
public class Manga {
	Connection conn=null;
	ResultSet rs;
	Statement stmt;
	public Manga(){
		try{
			Class.forName("org.sqlite.JDBC");
			conn =DriverManager.getConnection("jdbc:sqlite:C:\\sqlite\\m_c.db");
			stmt = conn.createStatement();
		}catch(SQLException | ClassNotFoundException e){
			e.printStackTrace();
		}
	}
	public List<String> getMangaInfo(String mangaName){
		List<List<String>> mangas = new ArrayList<List<String>>();
		//List<String> chapters = new ArrayList<String>();
		List<String> info;
		List<String> manga_info = new ArrayList<String>();
		try {
			rs = stmt.executeQuery("SELECT * FROM Series JOIN Mangaka ON "
					+ "\"Mangaka\".\"Manga ID\" = \"Series\".\"Manga ID\""
					+ " WHERE  Name=\"" +mangaName + "\" LIMIT 1");
			while(rs.next()){
				info = new ArrayList<String>();
				info.add(rs.getString("Cover"));
				info.add(rs.getString("Name"));
				info.add(rs.getString("Alternate"));
				info.add(rs.getString("Release"));
				info.add(rs.getString("Status"));
				info.add(rs.getString("Author"));
				info.add(rs.getString("Artist"));
				info.add(rs.getString("Desc"));
				info.add(rs.getString("Reading"));
				info.add(rs.getString("Genres"));
				mangas.add(info);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return manga_info;
	}
	
	public int Random(){
		Random randomGenerator = new Random();
		List<List<String>> mangas = new ArrayList<List<String>>();
		//List<String> Chapters = new ArrayList<String>();
		List<String> info;
		int mangaID=1;
		boolean empty = true;
		try {
			rs = stmt.executeQuery("SELECT COUNT(*) FROM Series");
			while(rs.next()){
				mangaID = rs.getInt("COUNT");
			}
			int selection = randomGenerator.nextInt(mangaID) +1;
			rs = stmt.executeQuery("SELECT * FROM Series JOIN Mangaka ON "
					+ "\"Mangaka\".\"Manga ID\" = \"Series\".\"Manga ID\""
					+ " WHERE  \"Manga ID=\"" + selection + "\"");
			while(rs.next()){
				empty = false;
				info = new ArrayList<String>();
				info.add(rs.getString("Cover"));
				info.add(rs.getString("Name"));
				info.add(rs.getString("Alternate"));
				info.add(rs.getString("Release"));
				info.add(rs.getString("Status"));
				info.add(rs.getString("Author"));
				info.add(rs.getString("Artist"));
				info.add(rs.getString("Desc"));
				info.add(rs.getString("Reading"));
				info.add(rs.getString("Genres"));
				mangas.add(info);
			}
			if(empty)
				Random();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}
	
	public int Random(String genre){
		Random randomGenerator = new Random();
		List<Integer> mangaID = new ArrayList<Integer>();
		List<List<String>> mangas = new ArrayList<List<String>>();
		//List<String> Chapters = new ArrayList<String>();
		List<String> info;
		try {
			rs = stmt.executeQuery("SELECT \"Manga ID\" FROM Series WHERE Genres LIKE %" + genre + "%");
			while(rs.next()){
				mangaID.add(rs.getInt("Manga ID"));
			}
			int selection = randomGenerator.nextInt(mangaID.size());
			rs = stmt.executeQuery("SELECT * FROM Series JOIN Mangaka ON "
					+ "\"Mangaka\".\"Manga ID\" = \"Series\".\"Manga ID\""
					+ " WHERE  \"Manga ID=\"" +mangaID.get(selection) + "\"");
			while(rs.next()){
				info = new ArrayList<String>();
				info.add(rs.getString("Cover"));
				info.add(rs.getString("Name"));
				info.add(rs.getString("Alternate"));
				info.add(rs.getString("Release"));
				info.add(rs.getString("Status"));
				info.add(rs.getString("Author"));
				info.add(rs.getString("Artist"));
				info.add(rs.getString("Desc"));
				info.add(rs.getString("Reading"));
				info.add(rs.getString("Genres"));
				mangas.add(info);
			}
			
					
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}
	
}
