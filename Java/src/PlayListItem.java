import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class PlayListItem {
	private String title;
	private int currentChapter;
	private int totalChapters;
	public PlayListItem(String title, int currentChapter){
		this.setTitle(title);
		this.setCurrentChapter(currentChapter);
		this.setTotalChapters();
		
	}
	public void setTitle(String title){
		this.title = title;
	}
	
	public String getTitle(){
		return this.title;
	}
	public int getCurrentChapter() {
		return currentChapter;
	}
	public void setCurrentChapter(int currentChapter) {
		this.currentChapter = currentChapter;
	}
	public int getTotalChapters() {
		return totalChapters;
	}
	public void setTotalChapters() {
		Connection c=null;
		try {
			c = DriverManager.getConnection("jdbc:sqlite:C:\\sqlite\\m_c.db");
			Statement stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT COUNT(*) FROM ");
			while(rs.next()){
				this.totalChapters = rs.getInt(0);
			}
			c.close();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
}
