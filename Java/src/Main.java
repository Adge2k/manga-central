import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

@SuppressWarnings("serial")
public class Main extends JFrame{
	JMenuBar menu = new JMenuBar();
	JPanel top = new JPanel();
	JPanel imagePanel = new JPanel();
	JPanel panel = new JPanel();
	JButton updateButton = new JButton("Update Manga List");
	JLabel text = new JLabel("This is where the text goes!!");
	JMenu fileMenu = new JMenu("File");
	JMenu updateMenu = new JMenu("Update");
	JMenu view = new JMenu("View");
	//ImageIcon img = new ImageIcon(pathToFileOnDisk);
	JScrollPane sb ;//= new JScrollPane();
	JMenuItem updateRecent;
	JMenuItem updateAll;
	JMenuItem exit;
	JMenuItem playList;
	JMenuItem mangaView;
	
	public Main(){
		super("Manga Central");
		JLabel picLabel = null;
		setSize(1400,800);
		setResizable(true);
		setJMenuBar(menu);
		BufferedImage myPicture = null;
		//ip = new ImagePanel();
		try{
			myPicture = ImageIO.read(new URL("http://i13.mangareader.net/chocolat/73/chocolat-5058539.jpg"));
			picLabel = new JLabel(new ImageIcon(myPicture));
		}catch(Exception e){
			
		}
		menu.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		menu.add(fileMenu);
		menu.add(updateMenu);
		menu.add(view);
		view.add(mangaView = new JMenuItem("Manga View"));
		view.add(playList = new JMenuItem("PlayList View"));
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		fileMenu.add(exit = new JMenuItem("exit"));
		updateMenu.add(updateRecent = new JMenuItem("Recent Updates -- quick"));
		updateMenu.add(updateAll = new JMenuItem("Complete Updates -- really slow"));
		exit.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				System.exit(0);
			}
		});
		updateRecent.addActionListener(new ActionListener() {
			 
		        public void actionPerformed(ActionEvent e)
		        {
		        	Connection c = null;
		    	    try {
					  Class.forName("org.sqlite.JDBC");
					  c = DriverManager.getConnection("jdbc:sqlite:C:\\sqlite\\m_c.db");
					  //c.setAutoCommit(false);
					  Update update = new Update(c);
					  new Thread(update).start();
					  //update.getNewManga();
					  //c.commit();
				    } catch ( Exception ex ) {
				      System.err.println( ex.getClass().getName() + ": " + ex.getMessage() );
				    }
		    }
		}); 
		updateAll.addActionListener(new ActionListener() {
			 
	        public void actionPerformed(ActionEvent e)
	        {
	        	Connection c = null;
	    	    try {
	    	      Class.forName("org.sqlite.JDBC");
			      c = DriverManager.getConnection("jdbc:sqlite:C:\\sqlite\\m_c.db");
			      //c.setAutoCommit(false);
			      Update update = new Update(c);
			      update.init();
			      //c.commit();
			    } catch ( Exception ex ) {
			      System.err.println( ex.getClass().getName() + ": " + ex.getMessage() );
			    }
	    	    JOptionPane.showMessageDialog(null, "Update Complete...");
		    }
		}); 
		menu.setBackground(Color.LIGHT_GRAY);
		panel.setBackground(Color.black);
		panel.setLayout(new GridLayout(2,1));
		panel.add(picLabel);
		JLabel t = new JLabel(new ImageIcon(myPicture));
		panel.add(t);
		//BoxLayout(panel, BoxLayout.LINE_AXIS));
		add(panel);
		this.getContentPane().add(sb = new JScrollPane(panel), BorderLayout.CENTER);
		sb.getVerticalScrollBar().setUnitIncrement(16);
		//add(sb);
		setVisible(true);
		
	}
	public static void main(String[] args) {
		new Main();
		
		

	}

}
