import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Scraper {
	int manID;
	int ChapID;
	String sql;
	Elements uls;
	Elements links;
	Connection c;
	Statement stmt;
	boolean checkpoint = false;
	
	public Scraper(Connection c){
		this.manID = 0;
		this.ChapID = 0;
		this.sql ="";
		this.c = c;
		
		try {
			c = DriverManager.getConnection("jdbc:sqlite:C:\\sqlite\\m_c.db");
			this.stmt = c.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}	
	}
	
	void init(){
		try{		
			Document doc = Jsoup.connect("http://www.mangareader.net/alphabetical").timeout(10*1000).get();
			Elements contents = doc.select("ul.series_alpha");
			for(Element content:contents){
				links = content.getElementsByTag("a");
				for(Element link:links){				
					if(link.text().compareTo("Undertaker Riddle")==0)
						checkpoint = true;
					if(checkpoint)
						getChapters(link.attr("href").toString(), link.text().replace('"', '\'').trim());
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	void getChapters(String link, String name){
		int MID =0;
		String altname, status, desc, yRelease, author, artist, direction, cover;
		altname = "";status="";desc ="";yRelease="";author="";artist="";direction="";
		String genre = "";
		Elements mangas;
		boolean empty = true;
		List<String> chapters = new ArrayList<String>();
		List<String> dateAdded = new ArrayList<String>();
		List<String> chLink = new ArrayList<String>();
		try {
			System.out.println(link);
			Document doc = Jsoup.connect("http://www.mangareader.net" + link).timeout(10*1000).get();
			ResultSet rs = stmt.executeQuery("SELECT * FROM Series WHERE Name = \"" + name + "\"");
			while(rs.next()){
				empty = false;
			}
			if (empty){
				Elements D = doc.getElementById("readmangasum").getElementsByTag("p");
				for (Element d: D){
					if (d.hasText())
						desc = d.text().replace('"', '\'');
				}
				Elements genreTags = doc.getElementsByClass("genretags");
				for(Element tag:genreTags){
					if (genre.compareTo("")!=0){
						genre += " , ";
					}
					genre += tag.text();
				}
				cover = doc.getElementById("mangaimg").select("img").attr("src");
				Elements properties = doc.getElementsByClass("propertytitle");
				for(Element property: properties){
					if (property.text().compareTo("Alternate Name:")==0){
						if (property.nextElementSibling().text().isEmpty()){
							altname = "NULL";
						}else{
							altname = property.nextElementSibling().text().replace('"', '\'');
						}
					}else if(property.text().compareTo("Year of Release:")==0){
						if (property.nextElementSibling().text().isEmpty()){
							yRelease = "NULL";
						}else{
							yRelease = property.nextElementSibling().text().replace('"', '\'');
						}
					}else if(property.text().compareTo("Status:")==0){
						if (property.nextElementSibling().text().isEmpty()){
							status = "NULL";
						}else{
							status = property.nextElementSibling().text().replace('"', '\'');
						}
					}else if(property.text().compareTo("Author:")==0){
						if (property.nextElementSibling().text().isEmpty()){
							author = "NULL";
						}else{
							author = property.nextElementSibling().text().replace('"', '\'');
						}
					}else if(property.text().compareTo("Artist:")==0){
						if (property.nextElementSibling().text().isEmpty()){
							artist = "NULL";
						}else{
							artist = property.nextElementSibling().text().replace('"', '\'');
						}
					}else if(property.text().compareTo("Reading Direction:")==0){
						if (property.nextElementSibling().text().isEmpty()){
							direction = "NULL";
						}else{
							direction = property.nextElementSibling().text().replace('"', '\'');
						}
					}
				}
				sql = "INSERT INTO Series VALUES( NULL, \"" + name + "\" , \"" + altname + "\", \"" + desc + "\", \"" + genre + "\", \"" + direction + "\", \"" + status + "\", 1 )";
				stmt.executeUpdate(sql);
				rs = stmt.executeQuery("SELECT \"Manga ID\" FROM Series WHERE Name = \"" + name + "\"");
				manID++;
				MID = rs.getInt("Manga ID");
				stmt.executeUpdate("INSERT INTO Mangaka VALUES(" +rs.getInt("Manga ID") + ", \"" + author + "\", \"" + artist + "\" , \"" + yRelease + "\" , \"" + link + "\" , \"" + cover + "\")");
			}
			Element mangasList = doc.getElementById("chapterlist");
			//System.out.println(mangasList);
			mangas = mangasList.getElementsByTag("td");
			for(Element manga: mangas){
				Elements M = manga.getElementsByTag("a");
				for (Element m: M){
					chapters.add(m.text().replace('"', '\''));
					chLink.add(m.attr("href"));
					dateAdded.add(m.parent().nextElementSibling().text());
				}
			}
			int chapter = 0;
			for(int i=0; i< chLink.size(); i++){
				rs = stmt.executeQuery("SELECT \"Chapter ID\" FROM Chapters WHERE \"Chapter Text\" = \"" + chapters.get(i) + "\"");
				empty = true;
				chapter++;
				while(rs.next()){
					empty = false;
				}
				if(empty){
					//System.out.println(chapters.get(i));
					ChapID++;
					getPages(chLink.get(i), chapters.get(i), dateAdded.get(i), MID, chapter);
					
				}
			}
			
		} catch (IOException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	
	void getPages(String link, String name, String date, int MID, int chapter){
		List<String> pages = new ArrayList<String>();
		String sqlInsert = "INSERT INTO Pages VALUES ";
		String img;
		try {
			Document doc = Jsoup.connect("http://www.mangareader.net" + link).timeout(10*1000).get();
			Elements values = doc.getElementsByTag("option");
			for(Element value:values){
				pages.add(value.attr("value"));
			}
			c.createStatement().executeUpdate("INSERT INTO Chapters VALUES ("+MID+", NULL, \"" + date + "\", \"" + name + "\", \"" + link + "\", " + pages.size() +"," + chapter +")");
			int ID = 0;
			ResultSet rs1=c.createStatement().executeQuery("SELECT \"Chapter ID\" FROM Chapters WHERE \"Chapter Text\" = \"" + name + "\"");
			while(rs1.next()){
				ID = rs1.getInt(1);
			}
			int count = 0;
			
			for(String page:pages){
				count++;
				doc = Jsoup.connect("http://www.mangareader.net" + page).timeout(10*1000).get();
				img = doc.getElementById("imgholder").getElementsByTag("img").attr("src");
				if(sqlInsert.compareTo("INSERT INTO Pages VALUES ")==0){
					sqlInsert+= "(NULL, "+ ID +", " + count + ", \"" + img.toString() + "\")";
				}else{
					sqlInsert+= ", (NULL, "+ ID +", " + count + ", \"" + img.toString() + "\")";
				}
				//stmt.executeUpdate("INSERT INTO Pages VALUES (NULL, "+ ID +", " + count + ", \"" + img.toString() + "\")");
				//System.out.println(img.toString());
			}
			stmt.executeUpdate(sqlInsert);
			//System.out.println(sqlInsert);
		} catch (IOException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
